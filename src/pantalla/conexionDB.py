import sqlite3
from sqlite3 import Error

def conexion_db ():
    try:
        con = sqlite3.connect('mydatabase.db')
        print('Conexion establecida')
        return con
    except Error:
        print ('Error')

def tabla_db(con,cursor_db):
    query_db(con,cursor_db,'CREATE TABLE IF NOT EXISTS AUTO (Marca text,Modelo text, Año integer, Combustible text, Patente text)')

def query_db (con,cursor_db,text):
    cursor_db.execute(text)
    con.commit()
    

def datos_db (cursor_db, text):
    cursor_db.execute(text)
    filas = cursor_db.fetchall()
    return filas

def datos_prueba(con,cursor_db,valores):
    cursor_db.execute('INSERT INTO AUTO(Marca,Modelo,Año,Combustible,Patente) VALUES(?,?,?,?,?)',valores)
    con.commit()
    print('se agrego nueva fila de datos')

def cierreConexion(con):
    con.close()





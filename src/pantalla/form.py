from tkinter import *
from tkinter import messagebox
import main

def eventoCargar():
    main.cargar(txtMarca.get(),txtModelo.get(),int(txtAño.get()),txtCombustible.get(),txtPatente.get())
       
    txtMarca.set('')
    txtModelo.set('')
    txtAño.set('')
    txtPatente.set('')
    txtCombustible.set('')

def eventoBuscar():
    data = main.buscar(txtPatente.get())
    if data is None:
        alerta()
    else:    
        txtMarca.set(data[0])
        txtModelo.set(data[1])
        txtAño.set(data[2])
        txtCombustible.set(data[3])
    entryDisabled()    

def eventoNuevo():
    txtMarca.set('')
    txtModelo.set('')
    txtAño.set('')
    txtPatente.set('')
    txtCombustible.set('')

def alerta ():
    messagebox.showinfo("NO ENCONTRADO", "LA PATENTE BUSCADA NO SE ENCUETRA EN LA BASE DE DATOS")


    

raiz=Tk()
txtMarca=StringVar()
txtModelo=StringVar()
txtAño=StringVar()
txtPatente=StringVar()
txtCombustible=StringVar()

raiz.title("Formulario")
miFrame= Frame()
miFrame.pack(fill="both", expand="True")
miFrame.config(width="400",height="500")

lblTitulo = Label(miFrame,text="Formulario", font=("Helvetica", 23)).place(x=130,y=30)

lblMarca= Label(miFrame,text="Marca:").place(x=50, y=100)
impMarca = Entry(miFrame,width=30,text="hola",state="disable", textvariable = txtMarca).place(x=150, y=100)

lblModelo= Label(miFrame,text="Modelo:").place(x=50, y=150)
impModelo = Entry(miFrame,width=30,textvariable = txtModelo).place(x=150, y=150)

lblAño= Label(miFrame,text="Año:").place(x=50, y=200)
impAño = Entry(miFrame,width=30,textvariable = txtAño).place(x=150, y=200)

lblPatente= Label(miFrame,text="Patente:").place(x=50, y=250)
impPatente = Entry(miFrame,width=30,textvariable = txtPatente).place(x=150, y=250)

lblCombustible= Label(miFrame,text="Combustible:").place(x=50, y=300)
impCombustible = Entry(miFrame,width=30,textvariable = txtCombustible).place(x=150, y=300)

btnCargar = Button(miFrame,text="Cargar",bg="grey",width=10,command = eventoCargar).place(x=50, y=400)

btnBuscar = Button(miFrame,text="Buscar",bg="grey",width=10,command = eventoBuscar).place(x=160, y=400)

btnNuevo = Button(miFrame,text="Nuevo",bg="grey",width=10, command = eventoNuevo ).place(x=270, y=400)

btnModificar = Button(miFrame,text="Modificar",bg="grey",width=10 ).place(x=50, y=450)

raiz.mainloop()


